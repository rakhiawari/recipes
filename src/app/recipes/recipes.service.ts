import { Injectable } from "@angular/core";
import { Recipe } from "./recipe.model";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class RecipesService {
  // private recipes: Recipe[] = [];
  private recipes: Recipe[] = [
    {
      id: "recipe1",
      title: "Pancakes",
      imageUrl:'https://www.kingarthurflour.com/sites/default/files/styles/featured_image/public/recipe_legacy/48-3-large.jpg?itok=inlusIOg',
      inggredients: ["All-purpose flour", "Milk", "Eggs", "Maple Syrup"]
    },
    {
      id: "recipe2",
      title: "Waffle",
      imageUrl:'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-keto-waffle-horizontal-034-1543784709.jpg?resize=768:*',
      inggredients: ["Flour","Egg","Buttermilk","Butter","Sugar","Baking Powder"],
    },
    {
      id: "recipe3",
      title: "Scrambled Eggs",
      imageUrl:
        "https://x9wsr1khhgk5pxnq1f1r8kye-wpengine.netdna-ssl.com/wp-content/uploads/Scrambled-with-Milk-930x620.jpg",
      inggredients: ["Butter", "eggs","Black Pepper"],
    },
    {
      id: "recipe4",
      title: "Tacos",
      imageUrl:'https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/001_Tacos_de_carnitas%2C_carne_asada_y_al_pastor.jpg/440px-001_Tacos_de_carnitas%2C_carne_asada_y_al_pastor.jpg',
      inggredients: ["Corn Tortilla", "Meat", "Veggies"],
    },
  ];
  constructor() {}

  getAllRecipes() {
    return [...this.recipes];
    // return this.http.get("src/assets/jsonData/recipe-data.json").map(res => res);
  }
  getRecipeById(recipeId: string) {
    return {
      ...this.recipes.find((recipe) => {
        return recipe.id == recipeId;
      }),
    };
  }
  deleteRecipe(recipeId: string) {
    this.recipes = this.recipes.filter(recipe => {
      return recipe.id != recipeId;
    });
  }
}
