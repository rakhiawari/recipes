import { RecipesService } from "./../recipes.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Recipe } from "../recipe.model";
import { AlertController } from '@ionic/angular';

@Component({
  selector: "app-recipe-detail",
  templateUrl: "./recipe-detail.page.html",
  styleUrls: ["./recipe-detail.page.scss"],
})
export class RecipeDetailPage implements OnInit {
  loadedRecipe: Recipe;
  constructor(
    private activatedRoute: ActivatedRoute,
    private recipesService: RecipesService,
    private router: Router,
    private alrtCtrl: AlertController
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.get("recipeId")) {
        this.router.navigate(['/recipes']);
      }
      let recipeId = paramMap.get("recipeId");
      this.loadedRecipe = this.recipesService.getRecipeById(recipeId);
    });
  }
  deleteRecipe() {
    this.alrtCtrl.create({
      header:"Are you sure?",
      message:"Are you sure you want to delete the recipe?",
      buttons:[
        {
          text:'Cancel',
          role:'cancel'
        },
        {
          text:'Delete',
          handler:() => {
            this.recipesService.deleteRecipe(this.loadedRecipe.id);
            console.log("Deleted");
            this.router.navigate(['/recipes']);
          }
        }
      ]
    }).then(alertEl => {
      alertEl.present();
    });
    
  }
}
